package com.supersmash.supersmashbrosserver.Controllers;

import com.supersmash.supersmashbrosserver.Models.FightersModel;
import com.supersmash.supersmashbrosserver.Models.GamesModel;
import com.supersmash.supersmashbrosserver.Repositories.FightersRepository;
import com.supersmash.supersmashbrosserver.Repositories.GamesRepository;
import com.supersmash.supersmashbrosserver.Service.FightersTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class FightersController {

    private final FightersRepository fightersRepo;
    private final GamesRepository gamesRepo;

    @Autowired
    private FightersTestService fightersTestService;

    private FightersController(FightersRepository fightersRepo, GamesRepository gamesRepo)
    {
        this.fightersRepo = fightersRepo;
        this.gamesRepo = gamesRepo;
    }

    @GetMapping(value = "/fighters", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FightersModel> getAllFighters() {
        return (List<FightersModel>)  fightersTestService.getAllFighters(); }

    @GetMapping("/fighter/{id}")
    public Optional<FightersModel> getCharacter(@PathVariable Long id) {return this.fightersRepo.findById(id); }

    @GetMapping("/gameTypes/{gameName}")
    public List<FightersModel> getFighterByGameType(@PathVariable String gameName) { return this.fightersRepo.getAllByGame_GameName(gameName);}

    @GetMapping("/fighterImages")
    public List<String> getAllImages() {return this.fightersRepo.getAllImages(); }
}
